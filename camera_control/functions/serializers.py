
from rest_framework import serializers
import uuid
from .models import *

class RecordSerializer(serializers.ModelSerializer):
    # Creat a Records form here:
    class Meta:
        model = Records
        fields = '__all__'
        
        
class FaceRecognitionSerializer(serializers.ModelSerializer):
    # Creat a FaceRecognitions form here:
    class Meta:
        model = FaceRecognitions
        fields = '__all__'