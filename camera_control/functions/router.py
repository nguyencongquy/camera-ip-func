from rest_framework.routers import DefaultRouter
from .viewsets import *


router = DefaultRouter()
router.register(r'record', RecordViewSet, basename='record')
router.register(r'face-recogition', FaceRecognitionViewSet, basename='facerecogition')
