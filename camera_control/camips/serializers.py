
from rest_framework import serializers
from .models import *

class CamIPSerializer(serializers.ModelSerializer):
    # Creat a CamIP form here:
    class Meta:
        model = CamIP
        fields = '__all__'