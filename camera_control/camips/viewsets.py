
import logging
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework import status


from camips.models import *
from functions.models import *
from .serializers import *

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .utils import *

# Logging
LOGGER = logging.getLogger('django')

class CamIpViewSet(viewsets.ModelViewSet):
    queryset = CamIP.objects.all()
    serializer_class = CamIPSerializer
        
        
    def list(self, request, *args, **kwargs):
        """_summary_

        Args:
            Get all camera infomation
        Returns:
            Camera information:{
                cam_id: UUID,
                cam_name: String,
                cam_ip: String,
                description: String
            }
        """
        queryset = self.queryset
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
    
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)