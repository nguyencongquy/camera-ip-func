from django.db import models
from django.utils.translation import gettext_lazy as _
import uuid


class CamIP(models.Model):
    cam_id = models.UUIDField(
        _("uuid"), primary_key=True, default=str(uuid.uuid4())
    )
    cam_name = models.CharField(max_length=100)
    cam_ip = models.GenericIPAddressField()
    created_at = models.DateTimeField(auto_now_add=True)
    cam_description = models.TextField()
    