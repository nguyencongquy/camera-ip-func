# Camera IPs management
1/ Install libraries:
    + pip install -r requirements.txt

2/ API Funtions:
    A/ Camera management
        + Get Camera profile: 
        + Create Camera profile
        + Remove Camera profile from DB
    B/ Functions:
        1. Records:
            a. By Schedule
                + Create Schedule and Save record
                + Remove Schedule from DB
            b. By Motions
                + Save record
            + Remove record

        2. Face Recognition.
            a. Check-in:
                + Add user: Train..
                + Detect user: Face Recogintion
                + Remove user: Remove user from DB