
import logging
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework import status


from camips.models import *
from functions.models import *
from .serializers import *

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .utils import *

# Logging
LOGGER = logging.getLogger('django')

class CamIpViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.queryset = CamIP.objects.all()
        self.serializer_class = CamIPSerializer
        
        
    def list(self, request):
        """_summary_

        Args:
            Get all camera infomation
        Returns:
            Camera information:{
                cam_id: UUID,
                cam_name: String,
                description: String
            }
        """
        queryset = self.queryset
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)