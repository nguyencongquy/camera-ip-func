
from rest_framework import serializers
import uuid
from .models import *

class CamIPSerializer(serializers.ModelSerializer):
    # Creat a CamIP form here:
    class Meta:
        model = CamIP
        fields = '__all__'