"""
URL configuration for camIP project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path,re_path,include

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator

from camips import router as camip_api_router
from functions import router as function_api_router


class BothHttpAndHttpsSchemaGenerator(OpenAPISchemaGenerator):
    def get_schema(self, request=None, public=False):
        schema = super().get_schema(request, public)
        schema.schemes = ["https", "http"]
        return schema
    
schema_view = get_schema_view(
   openapi.Info(
      title="Camera IP API",
      default_version='v0',
      description="Camera IP API description",
      terms_of_service="https://www.yourapp.com/terms/",
      contact=openapi.Contact(email="contact@yourapp.com"),
      license=openapi.License(name="Your License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
   generator_class=BothHttpAndHttpsSchemaGenerator,
)
api_url_patterns = [
    # Acounts
    # path(r'accounts/', include(users_api_router.router.urls)),
    # path(r'subscriber/', include(subscriber_api_router.router.urls)),
    # path(r'merchant/', include(merchant_api_router.router.urls)),
    # path(r'catalog/', include(catalog_api_router.router.urls)),
    # path(r'version/', include(version_api_router.router.urls)),
    # path(r'payment/', include(payment_api_router.router.urls)),
    # path(r'device/', include(device_api_router.router.urls)),
    # path(r'order/', include(order_api_router.router.urls)),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/',include(api_url_patterns)),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
]


if settings.DEBUG: 
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns.append(path('__debug__/', include('debug_toolbar.urls')))
