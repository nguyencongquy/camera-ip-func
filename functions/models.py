from django.db import models
from django.utils.translation import gettext_lazy as _
from camips.models import CamIP
import uuid

class Records(models.Model):
    record_id = models.UUIDField(
        _("uuid"), primary_key=True, default=str(uuid.uuid4())
    )
    record_name = models.CharField(max_length=100)
    
    record_description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    scheduled_at = models.DateTimeField()
    schedule = models.BooleanField()
    record_description = models.TextField()
    # Foreign Key Location
    cam_ip_fk = models.ForeignKey(CamIP, on_delete=models.CASCADE)


class FaceRecognitions(models.Model):
    face_id = models.UUIDField(
        _("uuid"), primary_key=True, default=str(uuid.uuid4())
    )
    face_name = models.CharField(max_length=100)
    
    face_description = models.TextField()
    check_in = models.DateTimeField()
    check_out = models.DateTimeField()
    face_description = models.TextField()
    # Foreign Key Location
    cam_ip_fk = models.ForeignKey(CamIP, on_delete=models.CASCADE)
