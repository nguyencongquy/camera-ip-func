
import logging
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework import status


from camips.models import *
from functions.models import *
from .serializers import *

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from .utils import *

# Logging
LOGGER = logging.getLogger('django')

class RecordViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.queryset = Records.objects.all()
        self.serializer_class = RecordSerializer
        
        
    def list(self, request):
        """_summary_

        Args:
            Get all records infomation
        Returns:
            Record information:{
                record_id: UUID,
                record_name: String,
                description: String
            }
        """
        queryset = self.queryset
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
    
    def create(self, request):
        """_summary_

        Args:
            Create a new record
        Returns:
            Record information:{
                record_id: UUID,
                record_name: String,
                description: String,
                time: DateTime,
                
            }
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FaceRecognitionViewSet(viewsets.ModelViewSet):
    def __init__(self):
        self.queryset = FaceRecognitions.objects.all()
        self.serializer_class = FaceRecognitionSerializer
        
        
    def list(self, request):
        """_summary_

        Args:
            Get all face recognition infomation
        Returns:
            Face recognition information:{
                face_id: UUID,
                face_name: String,
                description: String
            }
        """
        queryset = self.queryset
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)
    
    def create(self, request):
        """_summary_

        Args:
            Create a new face recognition
        Returns:
            Face recognition information:{
                face_id: UUID,
                face_name: String,
                description: String
            }
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)